import { PredictService } from './../predict.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  name: string;
  completed: boolean;
  


 restocheck;


 

  userId;

 
  customers:Customer[];
  customers$;
  addCustomerFormOpen=false;
  rowToEdit:number = -1; 
  customerToEdit:Customer = {name:null, years:null, income:null};

  list=[]; //for the checkboxxxxxxx
  isChecked=[]; ///for the checkboxxxxxxxxxx

  add(customer:Customer){
    this.customersService.addCustomer(this.userId, customer.name, customer.years, customer.income)
  }
  
  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.years = this.customers[index].years;
    this.customerToEdit.income = this.customers[index].income;
    this.rowToEdit = index; 
  }
  /**updateFromCheck(index){
    if(this.customers[index].result=='Will pay'){
      this.customers[index].result='Will default';
    }else{
      this.customers[index].result='Will pay';
    }
    this.customers[index].saved = true; 
    this.customersService.updateRsult(this.userId,this.customers[index].id,this.customers[index].result);

  }**/
  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.customersService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.years,this.customerToEdit.income);
    this.rowToEdit = null;
  }

 deleteSelected(){
   let pato;
   for( pato in this.list){
     if(this.list[pato]==true){
       this.customersService.deleteCustomer(this.userId,this.customers[pato].id)
     }
   }
 }

 checkSelected(i){
  this.isChecked[i]=!this.isChecked[i]
  if(this.isChecked[i]==true){
    this.list[i]=true;
  }else{
    this.list[i]=false;
  }
  
 }

  deleteCustomer(index){
    let id = this.customers[index].id;
    this.customersService.deleteCustomer(this.userId, id);
  }

  updateResult(index){
    this.customers[index].saved = true; 
    this.customersService.updateRsult(this.userId,this.customers[index].id,this.customers[index].result);
  }

  predict(index){
    this.customers[index].result = 'Will default';
    this.predictionService.predict(this.customers[index].years, this.customers[index].income).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will pay';
        } else {
          var result = 'Will default'
        }
        this.customers[index].result = result;
        this.restocheck=result;}
    );
    
    
  }

  displayedColumns: string[] = ['name', 'Education in years', 'Personal income','Delete', 'Edit', 'Predict', 'Result','Check'];
 
  constructor(private customersService:CustomersService,
    private authService:AuthService,
    private predictionService:PredictService ) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();
                if(customer.result){
                  customer.saved = true; 
                }
                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
              }                        
            }
          )
      })
  }   
}
